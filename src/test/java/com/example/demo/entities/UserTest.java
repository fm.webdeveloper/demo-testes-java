package com.example.demo.entities;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

    @Test
    public void UserTest() {
        User user = new User("name", 12, "DOC12345");
        assertEquals("name", user.getName());
        assertTrue(user.toString().contains(("User{")));
    }

    @Test
    public void UserIsNotMaiorDeIdade() {
        User user = new User("name", 12, "DOC12345");
        assertFalse(user.isMaior());
    }

    @Test
    public void UserIsMaiorDeIdade() {
        User user = new User("name", 18, "DOC12345");
        assertTrue(user.isMaior());
    }
}
